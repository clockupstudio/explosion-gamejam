// module for file-loader.

declare module "*.png" {
    const content: string;
    export default content;
}

declare module "*.json" {
    const content: string;
    export default content;
}

declare module "*.ogg" {
    const content: string;
    export default content;
}
