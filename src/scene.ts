import blocksImage from '../sprites/blocks.png';
import bulletImage from '../sprites/bullet.png';
import doorImage from '../sprites/door.png';
import playerImage from '../sprites/player.png';
import stage01Tiled from '../tiles/stage_01.json';
import explosionImage from '../sprites/explosion.png';
import explosionSound from '../sounds/explode.ogg';

import {
  addAnimations,
  idle,
  movePlayer
} from './player';
import {
  addExplosionAnimations
} from './explosion';
import {
  backgroundColor
} from './color';
import {
  createFromObjectLayer,
  detectBlocksAroundPlayer,
  positionTrackFromBlocks,
} from './block';
import {
  LEFT,
  RIGHT,
  DOWN,
  NONE
} from './input'

const KeyCodes = Phaser.Input.Keyboard.KeyCodes;

/**
 * constructStage creating stage from tiledmap.
 * @param {Phaser.Scene} scene
 * @param {string} stageName
 */
function constructStage(scene, stageName) {
  const stage = scene.make.tilemap({
    key: stageName,
  });
  const tileset = stage.addTilesetImage('blocks');
  const boundary = stage.createStaticLayer('boundary', tileset, 0, 0);
  boundary.setCollisionByProperty({
    collide: true,
  });
  console.log(boundary);
  // scene.physics.world.convertTilemapLayer(boundary);
  return stage;
}

/**
 *
 * @param {Phaser.Scene} scene
 * @param {object} stage
 */
function constructBlocks(scene, group, stage) {
  let blocks = []
  stage.objects.forEach((layer) => {
    if (layer.name === 'blocks') {
      blocks = [...blocks, ...createFromObjectLayer(scene, layer)];
    }
  });
  blocks.forEach((block) => {
    group.add(block);
  })
  return blocks
}

function constructDoor(scene, stage) {
  let doors = [];
  stage.objects.forEach((layer) => {
    if (layer.name === 'door') {
      doors = [...doors, ...createFromObjectLayer(scene, layer)];
    }
  });
  return doors[0];
}

export const PlayScene = {
  preload() {
    this.load.image('blocks', blocksImage);
    this.load.image('bullet', bulletImage);
    this.load.image('door', doorImage);
    this.load.spritesheet('explosion', explosionImage, {
      frameWidth: 12,
      frameHeight: 12,
    });
    this.load.spritesheet('player', playerImage, {
      frameWidth: 12,
      frameHeight: 24,
    });

    this.load.tilemapTiledJSON('stage_01', stage01Tiled);

    this.load.audio('explosion', explosionSound);
  },

  create() {
    this._state = {
      pressedKey: NONE,
      // contains boxes around player.
      blocks: {
        left: -1,
        right: -1,
        down: -1,
      },
      player: idle(false),
      blockPositions: [],
    };

    this.textures.list.blocks.add('soft', 0, 36, 0, 12, 12);
    this.textures.list.blocks.add('normal', 0, 24, 0, 12, 12);
    this.textures.list.blocks.add('hard', 0, 12, 0, 12, 12);
    this.textures.list.blocks.add('steel', 0, 0, 0, 12, 12);

    this.cameras.main.setBackgroundColor(backgroundColor);

    this._blockGroup = this.add.group();

    const stage = constructStage(this, 'stage_01');
    this._blocks = constructBlocks(this, this._blockGroup, stage);
    this._door = constructDoor(this, stage);

    // initialize input.
    const downKey = this.input.keyboard.addKey(KeyCodes.DOWN);
    downKey.on('down', (_) => this._state.pressedKey = DOWN);
    downKey.on('up', (_) => this._state.pressedKey = NONE);

    const rightKey = this.input.keyboard.addKey(KeyCodes.RIGHT);
    rightKey.on('down', (_) => this._state.pressedKey = RIGHT);
    rightKey.on('up', (_) => this._state.pressedKey = NONE);

    const leftKey = this.input.keyboard.addKey(KeyCodes.LEFT);
    leftKey.on('down', (_) => this._state.pressedKey = LEFT);
    leftKey.on('up', (_) => this._state.pressedKey = NONE);

    this.player = this.physics.add.sprite(84 - 6, 80, 'player');
    this.physics.add.collider(this.player, this._blockGroup);
    this.physics.add.collider(this.player, stage.layer.tilemapLayer);

    this.physics.add.overlap(this.player, this._door, () => {
      setTimeout(() => {
        this.scene.restart();
      }, 300);
    });

    addAnimations(this);
    addExplosionAnimations(this);

    this._explosionSound = this.sound.add('explosion');
  },

  update() {
    this._state.blockPositions = positionTrackFromBlocks(this._blocks);
    // TODO: this suppose to be rename to another name.
    this._state.blocks = detectBlocksAroundPlayer(this._state.blockPositions, {
      type: 'DETECT_BLOCKS',
      playerPosition: [this.player.x, this.player.y],
    })
    this._state.player = movePlayer(this._state.player, {
      type: 'MOVE',
      direction: this._state.pressedKey
    });

    const destroyBlockId = detectDestroyingBlock(this._state);
    if (destroyBlockId >= 0 && --this._blocks[destroyBlockId].hp === 0 && this._blocks[destroyBlockId].anims.getCurrentKey() !== 'explode') {
      this._blocks[destroyBlockId].anims.play('explode');
      this._explosionSound.play();
      this.cameras.main.shake(100, .01);
      this.cameras.main.flash(100);
      setTimeout(() => {
        this._blocks[destroyBlockId].destroy();
        this._blocks.splice(destroyBlockId, 1);
      }, 100);
    }

    this.player.setVelocityX(this._state.player.velocity);
    this.player.anims.play(this._state.player.animation.key, this._state.player.animation.ignoreIfPlaying);
    this.player.flipX = this._state.player.flipX;
  },
};

/**
 * getting id that want to destroy the block.
 *
 * TODO: it's needs to have a better way to getting data from BlockPositionTrack.
 */
function detectDestroyingBlock(state) {
  switch (state.pressedKey) {
    case DOWN:
      if (state.blocks.bottom.length > 0) {
        return state.blocks.bottom[0][0];
      }
      break;
    case LEFT:
      if (state.blocks.left.length > 0) {
        return state.blocks.left[0][0];
      }
      break;
    case RIGHT:
      if (state.blocks.right.length > 0) {
        return state.blocks.right[0][0];
      }
      break;
  }
  return -1;
}
