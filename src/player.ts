import { RIGHT, LEFT, DOWN } from './input'

export function addAnimations(scene: Phaser.Scene): void {
  scene.anims.create({
    key: "idle",
    frames: scene.anims.generateFrameNumbers("player", {
      start: 0,
      end: 0
    }),
    frameRate: 1,
    repeat: -1
  });

  scene.anims.create({
    key: "walk",
    frames: scene.anims.generateFrameNumbers("player", {
      start: 0,
      end: 1
    }),
    frameRate: 6,
    repeat: -1
  });

  scene.anims.create({
    key: "dead",
    frames: scene.anims.generateFrameNumbers("player", {
      start: 2,
      end: 2
    }),
    frameRate: 1,
    repeat: -1
  });

  scene.anims.create({
    key: "bomb_side",
    frames: scene.anims.generateFrameNumbers("player", {
      start: 3,
      end: 3
    }),
    frameRate: 1,
    repeat: -1
  });

  scene.anims.create({
    key: "bomb_bottom",
    frames: scene.anims.generateFrameNumbers("player", {
      start: 4,
      end: 4
    }),
    frameRate: 1,
    repeat: -1
  });
}

type PlayerState = {
  velocity: number,
  animation: {
    key: 'walk' | 'idle',
    ignoreIfPlaying: boolean,
  },
  flipX: boolean,
}

type MoveAction = {
  type: 'MOVE',
  direction: 'LEFT' | 'RIGHT',
}

const MOVE_SPEED = 50;

export function moveRight(): PlayerState {
  return {
    velocity: MOVE_SPEED,
    animation: {
      key: 'walk',
      ignoreIfPlaying: true,
    },
    flipX: false,
  };
}

export function moveLeft(): PlayerState {
  return {
    velocity: -MOVE_SPEED,
    animation: {
      key: 'walk',
      ignoreIfPlaying: true,
    },
    flipX: true,
  };
}

export function idle(flipX: boolean): PlayerState {
  return {
    velocity: 0,
    animation: {
      key: 'idle',
      ignoreIfPlaying: false,
    },
    flipX: flipX
  };
}

/**
 * Update move player state.
 * @param state
 * @param action
 */
export function movePlayer(state: PlayerState, action: MoveAction): PlayerState {
  switch (action.direction) {
    case LEFT:
      return moveLeft();
    case RIGHT:
      return moveRight();
    default:
      return idle(state.flipX);
  }
}
