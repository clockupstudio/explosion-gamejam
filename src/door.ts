import Phaser from 'phaser';

export function door(scene: Phaser.Scene, x: number, y: number): Door {
  const d = new Door(scene, x, y, 1);
  scene.add.existing(d);
  scene.physics.add.existing(d, true);
  return d
}

class Door extends Phaser.GameObjects.Sprite{
  public hp: number;

  constructor(scene: Phaser.Scene, x:number, y:number, hp: number) {
    super(scene, x, y, 'door');

    this.hp = hp;
  }
}
