import Phaser from 'phaser';
import { door } from './door';

/**
 * construct block from tiledmap layer.
 * @param {Phaser.Scene} scene
 * @param {object} layer
 */
export function createFromObjectLayer(scene: Phaser.Scene, layer) {
  return layer.objects.map((obj) => {
    const physics = scene;
    let [x, y] = [obj.x + 6, obj.y - 6];
    switch (obj.gid) {
      case 5:
        return door(physics, x, y);
      case 4:
        return softBlock(physics, x, y);
      case 3:
        return normalBlock(physics, x, y);
      case 2:
        return hardBlock(physics, x, y);
      case 1:
        return steelBlock(physics, x, y);
      default:
        return undefined;
    }
  });
}

type BlockKind = 'soft' | 'normal' | 'hard' | 'steel';

export function softBlock(scene: Phaser.Scene, x: number, y: number) {
  return block(scene, x, y, 'soft', 1);
}

export function normalBlock(scene: Phaser.Scene, x: number, y: number) {
  return block(scene, x, y, 'normal', 3);
}

export function hardBlock(scene: Phaser.Scene, x: number, y: number) {
  return block(scene, x, y, 'hard', 5);
}

export function steelBlock(scene: Phaser.Scene, x: number, y: number) {
  return block(scene, x, y, 'steel', -1);
}

function block(scene: Phaser.Scene, x: number, y: number, kind: BlockKind, hp: number): Block {
  const b = new Block(scene, x, y, kind, hp);
  scene.add.existing(b);
  scene.physics.add.existing(b, true);
  return b;
}

class Block extends Phaser.GameObjects.Sprite {
  public hp: number;
  public readonly kind: BlockKind;

  constructor(scene: Phaser.Scene, x: number, y: number, kind: BlockKind, hp: number) {
    super(scene, x, y, 'blocks', kind);

    this.hp = hp;
    this.kind = kind;
  }
}

/**
 * Position represents [x, y] in array format.
 */
type Position = [number, number]

type BlockPositionTrack = [number, Position]

type BlockAroundPlayerState = {
  left: BlockPositionTrack[],
  right: BlockPositionTrack[],
  bottom: BlockPositionTrack[],
}

type DetectBlocksAction = {
  type: 'DETECT_BLOCKS',
  playerPosition: Position,
}

function sameLine(blockPosition: Position, playerPosition: Position): boolean {
  let [by, py] = [blockPosition[1], playerPosition[1]];
  return (by > py && by - py <= 6);
}

function leftBlock(blockPosition: Position, playerPosition: Position): boolean {
  let [bx, px] = [blockPosition[0], playerPosition[0]];
  return (bx < px && (px - bx <= 12))
    && sameLine(blockPosition, playerPosition);
}

function rightBlock(blockPosition: Position, playerPosition: Position): boolean {
  let [bx, px] = [blockPosition[0], playerPosition[0]];
  return (bx > px && (px - bx <= 12))
    && sameLine(blockPosition, playerPosition);
}

function bottomBlock(blockPosition: Position, playerPosition: Position): boolean {
  let [bx, by] = blockPosition;
  let [px, py] = playerPosition;
  return Math.abs(bx - px) <= 3 && Math.abs(by - py) <= 18;
}

export function detectBlocksAroundPlayer(blocks: BlockPositionTrack[], action: DetectBlocksAction): BlockAroundPlayerState {
  return {
    left: blocks.filter((block) => leftBlock(block[1], action.playerPosition)),
    right: blocks.filter((block) => rightBlock(block[1], action.playerPosition)),
    bottom: blocks.filter((block) => bottomBlock(block[1], action.playerPosition)),
  }
}

export function positionTrackFromBlocks(blocks: Block[]): BlockPositionTrack[] {
  return blocks
    // we needs to filter block that already destroyed out of our interest blocks.
    .filter((b) => b.body)
    .map((b, i) => [i, [b.x, b.y]]);
}
