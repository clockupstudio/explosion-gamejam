/**
 * Reducer is function for produce new state from previous state.
 */
type Reducer<S, A> = (state: S, action: A) => S;
