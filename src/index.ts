import Phaser from 'phaser';
import { PlayScene } from './scene';

new Phaser.Game({
  type: Phaser.AUTO,
  width: 144,
  height: 288,
  physics: {
    default: 'arcade',
    arcade: {
      gravity: { y: 50, },
      debug: DEBUG,
    },
  },
  scene: PlayScene,
  zoom: 4,
  scale: {
    mode: Phaser.Scale.FIT,
    parent: 'main',
    autoCenter: Phaser.Scale.CENTER_BOTH,
  },
  render: {
    pixelArt: true,
  }
});
