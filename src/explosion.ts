import Phaser from 'phaser';

export function addExplosionAnimations(scene: Phaser.Scene) {
  scene.anims.create({
    key: "explode",
    frames: scene.anims.generateFrameNumbers("explosion", {
      start: 0,
      end: 0
    }),
    frameRate: 1,
    repeat: 0
  });
}
