module.exports = {
  'env': {
    'browser': true,
    'es6': true,
  },
  'extends': [
    'google',
  ],
  'globals': {
    'Atomics': 'readonly',
    'SharedArrayBuffer': 'readonly',
  },
  'parserOptions': {
    'ecmaVersion': 2018,
    'sourceType': 'module',
  },
  'rules': {
    'comma-dangle': ['warn', {
      'objects': 'always',
      'arrays': 'always',
    }],
    'object-curly-spacing': ['warn', 'always'],
    'eol-last': ['warn', 'always']
  },
};
