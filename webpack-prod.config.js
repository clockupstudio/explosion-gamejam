const TerserPlugin = require('terser-webpack-plugin')
const merge = require('webpack-merge')
const base = require('./webpack-base.config')

module.exports = merge(base, {
  mode: 'production',
  optimization: {
    splitChunks: {
      chunks: 'all',
      minSize: 30000,
      name: true,
      cacheGroups: {
        vendors: {
          test: /[\\/]node_modules[\\/]/,
          priority: -10,
        }
      }
    },
    minimizer: [new TerserPlugin({
      cache: true,
      parallel: true,
    })]
  }
});
